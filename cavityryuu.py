from __future__ import division
import numpy as np
import math
import datetime
from numba import jit

def main():
    nx = 50
    ny = 50
    dx = 0.002
    dy = 0.002
    ddx = 1.0/dx
    ddy = 1.0/dy
    ddx2 = (1/(dx**2))
    ddy2 = (1/(dy**2))
    Re = 1000
    dt = 0.001
    #dt = min(dt,0.25*dx)#kuuran suu
    dt = min(dt,0.2*Re*dx*dx)#kakusan suu
    #dt = dt/2.0
    # # Re>70 due to the method to compute difference.
    times = 300  #  repeat times

    u = np.zeros((nx+1, ny+2))  
    v = np.zeros((nx+2, ny+1))
    up = np.zeros((nx+1, ny+2))
    vp = np.zeros((nx+2, ny+1))
    p = np.zeros((nx+2, ny+2))  #with dummy mesh
    phi = np.zeros((nx+2, ny+2))
    #prephi = np.zeros((nx+2, ny+2))
    divup = np.zeros((nx+1, ny+1))
    #div = 0
    t = 10000

    for value in range(t):
        up = PrecalU(dx,dy,dt,nx,ny,u,v,up,vp,p,Re)
        vp = PrecalV(dx,dy,dt,nx,ny,u,v,up,vp,p,Re)
        divup = Divcal(dx,dy,nx,ny,up,vp)
        phi[:,:] = 0
        phi = SolvePossion(times,divup,ddx,ddy,phi,ddx2,ddy2,nx,ny,dt)

        phi[1][1] = 0
        phi[0,1:ny] = phi[1,1:ny]
        phi[nx+1,1:ny] = phi[nx,1:ny]
        phi[1:nx,0] = phi[1:nx,1]
        phi[1:nx,ny+1] = phi[1:nx,ny]
        phi[1][1] = 0
        
        u = up-(np.diff(phi,1,0)*dt)/dx
        
        #b.c.
        u[:,ny+1] = 2-u[:,ny]
        u[:,0] = -u[:,1]
        u[0,:] = 0
        u[nx,:] = 0
        
        v = vp-(np.diff(phi)*dt)/dy
        
        # b.c.
        v[:,ny] = 0
        v[:,0] = 0
        v[0,:] = -v[1,:]
        v[nx+1,:] = -v[nx,:]
        
        p = p + phi

        if value%100==0:
            OutputVtk(nx,ny,u,v,p,value)
            print "draw T = {}".format(value), "at",datetime.datetime.now()

@jit('f8[:,:](f8,f8,f8,i1,i1,f8[:,:],f8[:,:],f8[:,:],f8[:,:],f8[:,:],f8)')
def PrecalU(dx,dy,dt,nx,ny,u,v,up,vp,p,Re):
    for j in range(1, ny+1):  #  sth wrong in circulation from this line
    	for i in range(1, nx):
            #Pre-Calculate for uP
            cnvU = (1.0/dx)*(((u[i+1][j]+u[i][j])/2.0)**2.0 - ((u[i][j]+u[i-1][j])/2.0)**2.0) + (1.0/dy)*(((u[i][j+1]+u[i][j])/2.0) * ((v[i+1][j]+v[i][j])/2.0) - ((u[i][j-1]+u[i][j])/2.0) * ((v[i+1][j-1]+v[i][j-1])/2.0))
            #vij=(v[i,j]+v[i,j-1]+v[i+1,j]+v[i+1,j+1])*0.25
            #cnvU = 0.5*ddx*(u[i,j]*(u[i+1,j]-u[i-1,j])-abs(u[i,j])*(u[i-1,j]-2.*u[i,j]+u[i+1,j]))+0.5*ddy*(vij*(u[i,j+1]-u[i,j-1]) -abs(vij)*(u[i,j-1]-2.*u[i,j]+u[i,j+1]))
            difU = ((u[i-1][j]-2.0*u[i][j]+u[i+1][j])/dx**2.0)+((u[i][j-1]-2.0*u[i][j]+u[i][j+1])/dy**2.0)
            up[i][j] = u[i][j] + dt * (-((p[i+1][j]-p[i][j])/dx)-cnvU + (1.0/Re)*difU)
    up[:,ny+1] = 2-up[:,ny]
    up[:,0] = -up[:,1]
    up[0,:] = 0
    up[nx,:] = 0
    return up

@jit('f8[:,:](f8,f8,f8,i1,i1,f8[:,:],f8[:,:],f8[:,:],f8[:,:],f8[:,:],f8)')
def PrecalV(dx,dy,dt,nx,ny,u,v,up,vp,p,Re):
    for j in range(1,ny):  #  sth wrong in circulation from this line
    	for i in range(1, nx+1):
            #Pre-Calculate for vP
            cnvV = (1.0/dy)*(((v[i][j]+v[i][j+1])/2.0)**2.0-((v[i][j-1]+v[i][j])/2.0)**2.0) + (1.0/dx)*( ((u[i][j]+u[i][j+1])/2.0) * ((v[i][j]+v[i+1][j])/2.0) - ((u[i-1][j]+u[i-1][j+1])/2.0)*((v[i-1][j]+v[i][j])/2.0))
            #uij=0.25*(u[i,j]+u[i+1,j]+u[i,j+1]+u[i+1,j+1])
            #cnvV=0.5*ddx*(uij*(v[i+1,j]-v[i-1,j])-abs(uij)*(v[i-1,j]-2.*v[i,j]+v[i+1,j]))+0.5*ddy*(v[i,j]*(v[i,j+1]-v[i,j-1])-abs(v[i,j])*(v[i,j-1]-2.*v[i,j]+v[i,j+1]))
            difV = ((v[i-1][j]-2.0*v[i][j]+v[i+1][j])/dx**2.0)+((v[i][j-1]-2.0*v[i][j]+v[i][j+1])/dy**2.0)
            vp[i][j] = v[i][j] + dt * (-((p[i][j+1]-p[i][j])/dy)-cnvV + (1.0/Re)*difV)
    vp[:,ny] = 0
    vp[:,0] = 0
    vp[0,:] = -vp[1,:]
    vp[nx+1,:] = -vp[nx,:]
    return vp

@jit('f8[:,:](f8,f8,i1,i1,f8[:,:],f8[:,:])')
def Divcal(dx,dy,nx,ny,up,vp):
    divup = np.zeros((nx+1, ny+1))
    for ia in range(1,nx):
        for ja in range(1,ny):
            divup[ia][ja] = (1.0/dx)*(up[ia][ja]-up[ia-1][ja])+(1.0/dy)*(vp[ia][ja]-vp[ia][ja-1])
    return divup

def OutputVtk(nx,ny,u,v,p,value):
    with open("d:\\testfile{}.vtk".format(value),"w") as vtkFile:
        xy = (nx+1)*(ny+1)
        vtkFile.write("# vtk DataFile Version 2.0\n")
        vtkFile.write("result in time {}\n".format(value))
        vtkFile.write("ASCII\n")
        vtkFile.write("DATASET STRUCTURED_GRID\n")
        vtkFile.write("DIMENSIONS {} {} 1\n".format(nx+1,ny+1))
        vtkFile.write("POINTS {} int\n".format(xy))
        for m in range(nx+1):
            for j in range(ny+1):
                vtkFile.write("{} {} 0\n".format(m,j))
        vtkFile.write("POINT_DATA {}\n".format(xy))
        #vtkFile.write("SCALARS Uvelocity float\n")
        #vtkFile.write("LOOKUP_TABLE default\n")
         #for i1 in range(xy):
            #vtkFile.write("{}\n".format(u.flat[i1]))
        #vtkFile.write("SCALARS Vvelocity float\n")
        #vtkFile.write("LOOKUP_TABLE default\n")
        #for i2 in range(xy):
            #vtkFile.write("{}\n".format(v.flat[i2]))
        vtkFile.write("SCALARS VelocityMagnitude float\n")
        vtkFile.write("LOOKUP_TABLE default\n")
        for i3 in range(0,xy):
            vtkFile.write("{}\n".format(math.sqrt(v[:ny+1,:].flat[i3]**2+u[:,1:].flat[i3]**2)))
        vtkFile.write("VECTORS velocity float\n")
        for i4 in range(0,xy):
            vtkFile.write("{} {} 0\n".format(u[:,1:].flat[i4],v[:,:ny+1].flat[i4]))

@jit('f8[:,:](i1,f8[:,:],f8,f8,f8[:,:],f8,f8,i1,i1,f8)')        
def SolvePossion(times,divup,ddx,ddy,phi,ddx2,ddy2,nx,ny,dt):
    for time in range(times):
        for ii in range(1,nx+1):
            for jj in range(1,ny+1):
                rhs = (1.0/dt)*divup[ii][jj]
                resid = ddx2 * (phi[ii-1][jj] - 2.0*phi[ii][jj] + phi[ii+1][jj]) + ddy2 * (phi[ii][jj-1] - 2.0*phi[ii][jj] + phi[ii][jj+1]) - rhs
                dphi = 1.7 * resid/(2.0*(ddx2 + ddy2))
                phi[ii][jj] = phi[ii][jj] + dphi
    return phi


if __name__ == '__main__':
    main()